﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Building
{

    public class Portal : MonoBehaviour, IIntreactable
    {
        [SerializeField] private string scene = "Scene2";
        [SerializeField] private GameManager gm;

        public void Interact()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Warp);
            SoundManager.Instance.PlayBGM(SoundManager.Sound.BGM2);
            gm.ChangeScene(scene);
        }


    }
}
