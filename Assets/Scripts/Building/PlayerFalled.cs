﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Building
{
    public class PlayerFalled : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {

            var falled = other.gameObject.GetComponent<IIntreactable>();
            falled?.Interact();
        }
    }
}