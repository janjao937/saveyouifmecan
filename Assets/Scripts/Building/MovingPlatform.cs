﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Building
{
    public class MovingPlatform : MonoBehaviour
    {
        private Vector3 currentTaget;

        private float startDelay;

        [SerializeField] private Vector3[] position;
        [SerializeField] private int positionNumber = 0;

        [SerializeField] private float tolerance;
        [SerializeField] private float speed;
        [SerializeField] private float delay;

        [SerializeField] private bool auto;




        void Start()
        {
            if (position.Length > 0)
            {
                currentTaget = position[0];
            }

            tolerance = speed * Time.deltaTime;

        }

        // Update is called once per frame
        void Update()
        {
            if (transform.position != currentTaget)
            {
                MovePlatForm();
            }
            else
            {
                UpdateTarget();
            }
        }



        private void MovePlatForm()
        {
            Vector3 head = currentTaget - transform.position;
            transform.position += (head / head.magnitude) * speed * Time.deltaTime;
            if (head.magnitude < tolerance)
            {
                transform.position = currentTaget;
                startDelay = Time.time;
            }
        }


        private void UpdateTarget()
        {
            if (auto)
            {
                if (Time.time - startDelay > delay)
                {
                    NextPlatForm();
                }
            }
        }

        private void NextPlatForm()
        {
            positionNumber++;
            if (positionNumber >= position.Length)
            {
                positionNumber = 0;
            }
            currentTaget = position[positionNumber];
        }











    }
}
