﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Building
{
    public class BrigeControl : MonoBehaviour
    {
        [SerializeField] private float force;
        [SerializeField] private float mass;
        [SerializeField] private float accelation = 300;
        [SerializeField] private Rigidbody rb;



        public void Calculate()
        {
            force = mass * accelation;
            rb.AddForce(0, force, force);
        }


    }
}