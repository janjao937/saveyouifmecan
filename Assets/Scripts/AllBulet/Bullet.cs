﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllBullet
{
    public class Bullet : MonoBehaviour
    {
        [SerializeField] private int damage;

        [SerializeField] private Rigidbody rb;

        [SerializeField] private float force = 20f;

        public void Init()
        {
            Move();
        }

        private void Move()
        {
            rb.AddRelativeForce(0, 0, force * rb.mass);

            Destroy(gameObject, 2f);
        }

        private void OnTriggerEnter(Collider other)
        {
            var target = other.gameObject.GetComponent<IDamage>();
            target?.Attacked(damage);
        }
    }
}