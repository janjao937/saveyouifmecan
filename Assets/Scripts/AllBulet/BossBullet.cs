﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllBullet
{
    public class BossBullet : MonoBehaviour
    {
        [SerializeField] private int damage;
        private void OnTriggerEnter(Collider other)
        {
            var target = other.gameObject.GetComponent<IDamage>();
            target?.Attacked(damage);

            Destroy(gameObject);
        }


    }
}
