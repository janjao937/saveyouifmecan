﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

namespace AllEnemy
{

    public class Enemy : MainScriptCharecter
    {

        [SerializeField] private Rigidbody rb;
        [SerializeField] private Transform target;
        [SerializeField] private float moveSpeed;
        [SerializeField] private float howClose;
        [SerializeField] private int damage;


        private float distance;


        private void Awake()
        {
            target = GameObject.FindGameObjectWithTag("Player").transform;
        }
        private void Update()
        {
            Move();
        }


        private void Move()
        {
            distance = Vector3.Distance(target.position, transform.position);
            if (distance <= howClose)
            {
                transform.LookAt(target);
                rb.AddForce(transform.forward * moveSpeed);
            }

        }

        private void OnTriggerEnter(Collider other)
        {
            var target = other.gameObject.GetComponent<IDamage>();
            target?.Attacked(damage);
        }

        protected override void Die()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.EnemyDie);
            Destroy(gameObject);

        }



    }
}
