﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace AllEnemy
{

    public class Boss : MainScriptCharecter
    {
        [SerializeField] private Rigidbody rb;

        public bool BossDie { get; private set; } = false;
        protected override void Die()
        {
            base.Die();

            BossDie = true;
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.BossDie);
            Destroy(gameObject, 4);
        }


    }
}
