﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Coin
{

    public class Coin : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;
        [SerializeField] private float addTorqueForce = 20f;

        private void Update()
        {
            rb.AddTorque(0,addTorqueForce, 0);
        }


        private void OnTriggerEnter(Collider other)
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.GetCoins);
            var item = other.gameObject.GetComponent<IGetItem>();

            item?.GetCoin();
            Destroy(gameObject);
        }

    }
}
