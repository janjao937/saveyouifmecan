﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class PlayerSoundManager : MonoSingleton<PlayerSoundManager>
    {
        public AudioSource AudioSorceForAction;


        [SerializeField] private SoundClip[] soundClips;


        public enum Sound
        {
            Fire,
            Die,
            Jump,
        }

        [Serializable]
        public struct SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0, 1)] public float soundVolume;
        }



        public void Play(AudioSource audioSource, Sound sound)
        {

            var soundClip = GetAudioClip(sound);
            audioSource.clip = soundClip.audioClip;
            audioSource.volume = soundClip.soundVolume;
            audioSource.Play();
            audioSource.loop = false;

        }


        private SoundClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }

            return default(SoundClip);
        }
    }
}
