﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using TMPro;

namespace Manager
{ 
 public class GameManager : MonoBehaviour
    {
        
        [SerializeField] private Character player;
        [SerializeField] private GameObject UIPanel;
        [SerializeField] private Button next;

        [SerializeField] private GameObject scorePanel;
        
     

        // Text[]  Score 

        [SerializeField]private TextMeshProUGUI scoreText;

        [SerializeField] private TextMeshProUGUI finalScoreText;

        [SerializeField] private TextMeshProUGUI useSupperBulletText;
     

        private void Start()
        {

            
        }
        void Update()
        {

            CountScore();
            ActivePanel();
            SupperBulletShowText();


        }

    
        public void ChangeScene(string Scene)
        {
            SceneManager.LoadScene(Scene);
            DontDestory();
        }

        public void GoToManuClicked()
        {
           
            SceneManager.LoadScene("MainManu");
        }

        private void DontDestory()
        {
            DontDestroyOnLoad(gameObject);
            DontDestroyOnLoad(player);
            DontDestroyOnLoad(UIPanel);
           
            
        }

        private void CountScore()
        {
            scoreText.text = $"Score:{player.ScoreForSend}";


        }

        private void ActivePanel()
        {

            if(player.IsDie)
            {
                finalScoreText.text= $"Final Score:{player.ScoreForSend}";
                scorePanel.SetActive(true);
                
            }
           
        }

        private void SupperBulletShowText()
        {
            if(player.UseSupperBullet)
            {
                //supperBulletText.text = "You have supper bullet";

                useSupperBulletText.gameObject.SetActive(true);
            }

        }


        public void OnNextButtonClicked()
        {
            SceneManager.LoadScene("Die");
        }

      

    }

}
