﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class SoundManager : MonoSingleton<SoundManager>
    {
        public AudioSource AudioSorceForAction;

        [SerializeField] private AudioSource audioSource;
        [SerializeField] private SoundClip[] soundClips;


        public enum Sound
        {
            BGM,
            BGM2,
            TakeHit,
            BossFire,
            EnemyDie,
            BossDie,
            GetCoins,
            Warp,
            Win,
            BrigeButton,
            PushButton,


        }

        [Serializable]
        public struct SoundClip
        {
            public Sound sound;
            public AudioClip audioClip;
            [Range(0, 1)] public float soundVolume;
        }



        public void Play(AudioSource audioSource, Sound sound)
        {

            var soundClip = GetAudioClip(sound);
            audioSource.clip = soundClip.audioClip;
            audioSource.volume = soundClip.soundVolume;
            audioSource.Play();
            audioSource.loop = false;

        }

        public void PlayBGM(Sound _sound)
        {
            audioSource.loop = true;
            if (_sound == Sound.BGM)
            {
                Play(audioSource, Sound.BGM);
            }
            else if (_sound == Sound.BGM2)
            {
                Play(audioSource, Sound.BGM2);
            }

        }

        private SoundClip GetAudioClip(Sound sound)
        {
            foreach (var soundClip in soundClips)
            {
                if (soundClip.sound == sound)
                {
                    return soundClip;
                }
            }

            return default(SoundClip);
        }
    }
}