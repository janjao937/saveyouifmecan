﻿using HealthBarInGame;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class MainScriptCharecter : MonoBehaviour, IDamage
    {

        [SerializeField] private int hp = 100;
        [SerializeField] private HealthBar healthBar;


        private void Awake()
        {

            healthBar.SetBar(hp);
        }

        public void Attacked(int damage)
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.TakeHit);

            hp -= damage;

            healthBar.SetHealth(hp);
            if (hp > 0)
            {
                return;
            }
            Die();
        }

        protected virtual void Die()
        {
            //Implemented
        }
    }
}
