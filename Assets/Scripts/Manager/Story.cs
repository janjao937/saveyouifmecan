﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

namespace Manager
{

    public class Story : MonoBehaviour
    {


        public void OnNextWinClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PushButton);
            SceneManager.LoadScene("MainManu");
        }

        public void OnNextStoryClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PushButton);
            SceneManager.LoadScene("Scene1");
        }
    }
}

