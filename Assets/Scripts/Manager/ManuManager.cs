﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.SceneManagement;

namespace Manager
{
    public class ManuManager : MonoBehaviour
    {

        [SerializeField] private Button startButton;
        [SerializeField] private Button exitButton;

        // Start is called before the first frame update
        void Start()
        {
            SoundManager.Instance.PlayBGM(SoundManager.Sound.BGM);
            startButton.onClick.AddListener(OnStartButtonClicked);



        }

        // Update is called once per frame
        void Update()
        {

            var remainingPlayers = GameObject.FindGameObjectsWithTag("Player");
            foreach (var player in remainingPlayers)
            {
                Destroy(player);
            }

            var managerInGame = GameObject.FindGameObjectsWithTag("Manager");
            foreach (var manager in managerInGame)
            {
                Destroy(manager);
            }

        }

        private void OnStartButtonClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PushButton);
            SceneManager.LoadScene("Story");

        }

        public void OnExitButtonClicked()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.PushButton);
            Application.Quit();
        }



    }
}
