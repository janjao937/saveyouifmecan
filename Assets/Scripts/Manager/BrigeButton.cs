﻿using Building;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Manager
{
    public class BrigeButton : MonoBehaviour, IIntreactable
    {
        [SerializeField] private BrigeControl brige;

        public void Interact()
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.BrigeButton);
            brige.Calculate();
            Destroy(gameObject);
        }


    }
}
