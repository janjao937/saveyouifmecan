﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Princess
{

    public class PinkPrincess : MonoBehaviour
    {
        private void OnTriggerEnter(Collider other)
        {
            SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.Win);
            var wasHelped = other.gameObject.GetComponent<Character>();

            wasHelped?.Win();
        }
    }
}
