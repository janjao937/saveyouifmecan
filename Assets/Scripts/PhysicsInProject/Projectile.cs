﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhysicsInProject
{
    public class Projectile : MonoBehaviour
    {

        [SerializeField] private Rigidbody bullet;

        [SerializeField] private Transform shootPoint;

        [SerializeField] private double fireRate = 1;


        private Transform target;
        private float fireCounter = 0;


        // Start is called before the first frame update
        void Start()
        {
            target = GameObject.FindGameObjectWithTag("Player").GetComponent<Transform>();
        }

        // Update is called once per frame
        void Update()
        {
            LaunchProjectile();

        }

        private void LaunchProjectile()
        {
            // SoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, SoundManager.Sound.BossFire);
            target.transform.position = target.transform.position + Vector3.up * 0.03f;
            Vector3 Vo = CalculateVelocity(target.transform.position, shootPoint.position, 1f);

            transform.rotation = Quaternion.LookRotation(Vo);

            fireCounter += Time.deltaTime;
            if (fireCounter >= fireRate)
            {
                Rigidbody fire = Instantiate(bullet, shootPoint.position, Quaternion.identity);
                fire.velocity = Vo;
                fireCounter = 0;
            }



        }

        private Vector3 CalculateVelocity(Vector3 target, Vector3 origin, float time)
        {
            Vector3 distance = target - origin;
            Vector3 distanceXZ = distance;
            distanceXZ.y = 0;
            //Vector to scalar
            float distanceXZScalar = distanceXZ.magnitude;
            float distanceY = distance.y;

            float Vxz = distanceXZScalar / time;
            float Vy = (distanceY / time) + 0.5f * Mathf.Abs(Physics.gravity.y) * time;

            Vector3 result = distanceXZ.normalized;
            result *= Vxz;
            result.y = Vy;
            return result;
        }

    }
}
