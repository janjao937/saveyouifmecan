﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhysicsInProject
{
    public class AngularVelocity : MonoBehaviour
    {

        [SerializeField] Rigidbody rb;
        [SerializeField] private Vector3 angularVelocity;
        float speed = 3f;





        private void Interact()
        {
            rb.angularVelocity = angularVelocity * speed;


        }

        private void OnTriggerEnter(Collider other)
        {
            Interact();
        }

        private void OnTriggerExit(Collider other)
        {
            rb.angularVelocity = Vector3.zero;
        }

    }
}
