﻿using AllEnemy;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhysicsInProject
{
    public class ParallelAxisTheorem : MonoBehaviour
    {
        [SerializeField] private Rigidbody rb;

        [SerializeField] private Boss boss;

        // Update is called once per frame
        void FixedUpdate()
        {
            if (boss.BossDie)
            {
                rb.angularVelocity = rb.centerOfMass;
            }


        }
    }
}
