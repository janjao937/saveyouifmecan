﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace PhysicsInProject
{

    public class Torque : MonoBehaviour
    {
        [SerializeField] Rigidbody rb;
        [SerializeField] float forceTorque = 20f;
        void FixedUpdate()
        {
            rb.AddTorque(0, forceTorque, 0);
        }

    }
}
