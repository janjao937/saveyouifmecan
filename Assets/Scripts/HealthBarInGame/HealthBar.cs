﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace HealthBarInGame
{

    public class HealthBar : MonoBehaviour
    {
        [SerializeField] Slider slider;

        public void SetBar(int health)
        {
            slider.maxValue = health;
            slider.value = health;

        }

        public void SetHealth(int health)
        {
            slider.value = health;
        }
    }
}