﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace HealthBarInGame
{
    public class CameraLookHealthBar : MonoBehaviour
    {
        [SerializeField] private Transform cam;

        private void LateUpdate()
        {
            transform.LookAt(transform.position + cam.forward);
        }
    }
}
