﻿using UnityEngine;


public class InputManager : MonoBehaviour
{
    [SerializeField] PlayerController movement;
    [SerializeField] Character character;

    PlayerInput controls;
    PlayerInput.MomentActions movementAction;
    Vector2 horizontalInput;


    void Awake()
    {
        controls = new PlayerInput();
        movementAction = controls.Moment;

        movementAction.HorizontalMoment.performed += contex => horizontalInput = contex.ReadValue<Vector2>();

        movementAction.Jump.performed += contex => movement.OnJumpPress();

        movementAction.Fire.performed += contex => Fire();


    }

    private void Update()
    {
        movement.ReceiveInput(horizontalInput);
    }
    private void OnEnable()
    {
        controls.Enable();

    }

    private void OnDestroy()
    {
        controls.Disable();
    }

    private void Fire()
    {
        character.OnFire();
    }
}
