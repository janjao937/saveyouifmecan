﻿using Manager;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    [SerializeField] CharacterController controller;
    [SerializeField] float speed = 11f;
    Vector2 horizontalInput;

    [SerializeField] float jumpHigh = 3.5f;
    bool jump;
    [SerializeField] float gravity = Physics.gravity.y;
    [SerializeField] float mass = 1f;
    Vector3 verticalVelocity = Vector3.zero;
    [SerializeField] LayerMask groundMask;
    bool isGrounded;
    float w=0;

    

    private void Awake()
    {
         w= gravity * mass;
    }


    private void FixedUpdate()
    {       
        isGrounded = Physics.CheckSphere(transform.position, 0.1f, groundMask);

        if(isGrounded)
        {
            verticalVelocity.y = 0;
        }
        Vector3 horizontalVelocity = (transform.right * horizontalInput.x + transform.forward * horizontalInput.y) * speed;

        controller.Move(horizontalVelocity * Time.deltaTime);

        if(jump)
        {
            if(isGrounded)
            {
                verticalVelocity.y = Mathf.Sqrt(-2f * jumpHigh * w);

            }
            jump = false;
        }

        verticalVelocity.y += w * Time.deltaTime;
        controller.Move(verticalVelocity * Time.deltaTime);
    }

    public void ReceiveInput(Vector2 horizontalInput)
    {
        this.horizontalInput = horizontalInput;

        print(this.horizontalInput);
    }

    public void OnJumpPress()
    {
       
        Debug.Log("Jump");
        jump = true;
        PlayerSoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, PlayerSoundManager.Sound.Jump);
    }
}
