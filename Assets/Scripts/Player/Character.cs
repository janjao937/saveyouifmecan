﻿using AllBullet;
using Manager;
using UnityEngine;
using UnityEngine.SceneManagement;



public class Character : MainScriptCharecter, IGetItem, IIntreactable
    {
        [SerializeField] private int score = 0;

         [SerializeField] private Bullet characterBullet;

        [SerializeField] private Bullet supperBullet;

        

        [SerializeField] private Transform gunPoint;

        public bool IsDie { get; private set; } = false;

        public int ScoreForSend = 0;

        public bool UseSupperBullet { get; private set; } = false;


       

        protected override void Die()
        {
            base.Die();

            PlayerSoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, PlayerSoundManager.Sound.Die);
            IsDie = true;



        }


        public void OnFire()
        {
            PlayerSoundManager.Instance.Play(SoundManager.Instance.AudioSorceForAction, PlayerSoundManager.Sound.Fire);

            if (score >= 5)
            {
                UseSupperBullet = true;
               var bulletSupper = Instantiate(supperBullet, gunPoint.position, Quaternion.identity);
                bulletSupper.Init();
          

            }
            else
            {
            
            var bullet = Instantiate(characterBullet, gunPoint.position, Quaternion.identity);
             bullet.Init();
            
        }

        }

        private void OnTriggerEnter(Collider other)
        {
            var obj = other.gameObject.GetComponent<IIntreactable>();
            obj?.Interact();

        }



        public void Win()
        {
            SceneManager.LoadScene("Win");
        }



        public int GetCoin()
        {
            score++;

            SendScore();
            return score;
        }

        public int SendScore()
        {
            ScoreForSend = score;

            return ScoreForSend;
        }


        public void Interact()
        {
            IsDie = true;
        }



    }

