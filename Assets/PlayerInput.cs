// GENERATED AUTOMATICALLY FROM 'Assets/PlayerInput.inputactions'

using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.InputSystem;
using UnityEngine.InputSystem.Utilities;

public class @PlayerInput : IInputActionCollection, IDisposable
{
    public InputActionAsset asset { get; }
    public @PlayerInput()
    {
        asset = InputActionAsset.FromJson(@"{
    ""name"": ""PlayerInput"",
    ""maps"": [
        {
            ""name"": ""Moment"",
            ""id"": ""b54cf96e-33b7-4242-a007-04ab7cde85cc"",
            ""actions"": [
                {
                    ""name"": ""HorizontalMoment"",
                    ""type"": ""PassThrough"",
                    ""id"": ""57f7d398-89bf-4d24-898e-b652a73ce3a8"",
                    ""expectedControlType"": ""Vector2"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Jump"",
                    ""type"": ""Button"",
                    ""id"": ""bfc8c4e7-8349-4e4e-8ae7-d62bb4c49618"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                },
                {
                    ""name"": ""Fire"",
                    ""type"": ""Button"",
                    ""id"": ""90655643-d969-4460-a2b6-dc1e5ad3e1c8"",
                    ""expectedControlType"": ""Button"",
                    ""processors"": """",
                    ""interactions"": """"
                }
            ],
            ""bindings"": [
                {
                    ""name"": ""WASD"",
                    ""id"": ""5529e841-3df2-4671-bbeb-aec45b2d7333"",
                    ""path"": ""2DVector"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoment"",
                    ""isComposite"": true,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": ""up"",
                    ""id"": ""70f4d6e4-fce3-450a-acce-437875bc24bd"",
                    ""path"": ""<Keyboard>/w"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""down"",
                    ""id"": ""0d6ccee2-b3e9-493f-8891-d260d8fe066e"",
                    ""path"": ""<Keyboard>/s"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""left"",
                    ""id"": ""b8951a51-d569-4953-a55f-b181d390224f"",
                    ""path"": ""<Keyboard>/a"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": ""right"",
                    ""id"": ""3ce45630-9e6a-4cfc-80cc-b617e5fa906d"",
                    ""path"": ""<Keyboard>/d"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""HorizontalMoment"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": true
                },
                {
                    ""name"": """",
                    ""id"": ""de7306b8-9f23-439e-af3e-906cbadfd053"",
                    ""path"": ""<Keyboard>/space"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Jump"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                },
                {
                    ""name"": """",
                    ""id"": ""9f235d0e-ddca-4221-9459-a6bd38d916d6"",
                    ""path"": ""<Mouse>/leftButton"",
                    ""interactions"": """",
                    ""processors"": """",
                    ""groups"": """",
                    ""action"": ""Fire"",
                    ""isComposite"": false,
                    ""isPartOfComposite"": false
                }
            ]
        }
    ],
    ""controlSchemes"": []
}");
        // Moment
        m_Moment = asset.FindActionMap("Moment", throwIfNotFound: true);
        m_Moment_HorizontalMoment = m_Moment.FindAction("HorizontalMoment", throwIfNotFound: true);
        m_Moment_Jump = m_Moment.FindAction("Jump", throwIfNotFound: true);
        m_Moment_Fire = m_Moment.FindAction("Fire", throwIfNotFound: true);
    }

    public void Dispose()
    {
        UnityEngine.Object.Destroy(asset);
    }

    public InputBinding? bindingMask
    {
        get => asset.bindingMask;
        set => asset.bindingMask = value;
    }

    public ReadOnlyArray<InputDevice>? devices
    {
        get => asset.devices;
        set => asset.devices = value;
    }

    public ReadOnlyArray<InputControlScheme> controlSchemes => asset.controlSchemes;

    public bool Contains(InputAction action)
    {
        return asset.Contains(action);
    }

    public IEnumerator<InputAction> GetEnumerator()
    {
        return asset.GetEnumerator();
    }

    IEnumerator IEnumerable.GetEnumerator()
    {
        return GetEnumerator();
    }

    public void Enable()
    {
        asset.Enable();
    }

    public void Disable()
    {
        asset.Disable();
    }

    // Moment
    private readonly InputActionMap m_Moment;
    private IMomentActions m_MomentActionsCallbackInterface;
    private readonly InputAction m_Moment_HorizontalMoment;
    private readonly InputAction m_Moment_Jump;
    private readonly InputAction m_Moment_Fire;
    public struct MomentActions
    {
        private @PlayerInput m_Wrapper;
        public MomentActions(@PlayerInput wrapper) { m_Wrapper = wrapper; }
        public InputAction @HorizontalMoment => m_Wrapper.m_Moment_HorizontalMoment;
        public InputAction @Jump => m_Wrapper.m_Moment_Jump;
        public InputAction @Fire => m_Wrapper.m_Moment_Fire;
        public InputActionMap Get() { return m_Wrapper.m_Moment; }
        public void Enable() { Get().Enable(); }
        public void Disable() { Get().Disable(); }
        public bool enabled => Get().enabled;
        public static implicit operator InputActionMap(MomentActions set) { return set.Get(); }
        public void SetCallbacks(IMomentActions instance)
        {
            if (m_Wrapper.m_MomentActionsCallbackInterface != null)
            {
                @HorizontalMoment.started -= m_Wrapper.m_MomentActionsCallbackInterface.OnHorizontalMoment;
                @HorizontalMoment.performed -= m_Wrapper.m_MomentActionsCallbackInterface.OnHorizontalMoment;
                @HorizontalMoment.canceled -= m_Wrapper.m_MomentActionsCallbackInterface.OnHorizontalMoment;
                @Jump.started -= m_Wrapper.m_MomentActionsCallbackInterface.OnJump;
                @Jump.performed -= m_Wrapper.m_MomentActionsCallbackInterface.OnJump;
                @Jump.canceled -= m_Wrapper.m_MomentActionsCallbackInterface.OnJump;
                @Fire.started -= m_Wrapper.m_MomentActionsCallbackInterface.OnFire;
                @Fire.performed -= m_Wrapper.m_MomentActionsCallbackInterface.OnFire;
                @Fire.canceled -= m_Wrapper.m_MomentActionsCallbackInterface.OnFire;
            }
            m_Wrapper.m_MomentActionsCallbackInterface = instance;
            if (instance != null)
            {
                @HorizontalMoment.started += instance.OnHorizontalMoment;
                @HorizontalMoment.performed += instance.OnHorizontalMoment;
                @HorizontalMoment.canceled += instance.OnHorizontalMoment;
                @Jump.started += instance.OnJump;
                @Jump.performed += instance.OnJump;
                @Jump.canceled += instance.OnJump;
                @Fire.started += instance.OnFire;
                @Fire.performed += instance.OnFire;
                @Fire.canceled += instance.OnFire;
            }
        }
    }
    public MomentActions @Moment => new MomentActions(this);
    public interface IMomentActions
    {
        void OnHorizontalMoment(InputAction.CallbackContext context);
        void OnJump(InputAction.CallbackContext context);
        void OnFire(InputAction.CallbackContext context);
    }
}
